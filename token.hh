#ifndef TOKEN_HH_
#define TOKEN_HH_
#ifdef __EVENT_USE_THREADS__
#include <atomic>
#endif

class token
{
    public:
        token()
        {
            _id = next_id();
        }

        bool operator==(const token& other) const
        {
            return _id == other._id;
        }

        unsigned int get_id() const
        {
            return _id;
        }

    private:
        unsigned int next_id()
        {
#ifdef __EVENT_USE_THREADS__
            static std::atomic<unsigned int> next_id(0);
#else
            static unsigned int next_id = 0;
#endif

            return next_id++;
        }

        unsigned int _id = 0;
};

namespace std
{
    template<>
    struct hash<token>
    {
        using argument_type = token;
        using result_type = std::size_t;

        result_type operator()(argument_type const& t) const
        {
            std::hash<unsigned int> id_hash;

            return id_hash(t.get_id());
        }
    };
}

#endif /* TOKEN_HH_ */
