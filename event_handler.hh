#ifndef EVENT_HANDLER_HH_
#define EVENT_HANDLER_HH_
#include <functional>
#include "token.hh"

template<typename... argument_types>
class event_handler: public std::function<void(argument_types...)>
{
    public:
        using this_type = event_handler<argument_types...>;

        /**
         * Helper method to create an event_handler from a functor object easily
         *
         * @param functor The functor object to call when the event handler called
         * @return An event_handler object, with the functor wrapped into a callable with std::bind
         */
        template<typename functor_type>
        static this_type from_functor(functor_type& functor)
        {
            using function_type = decltype(&functor_type::operator());
            using binding_type = functor_type*;

            return bind<function_type, binding_type, argument_types...>(&functor_type::operator(), &functor);
        }

        /**
         * Helper method to create an event_handler from a class' method
         *
         * @param method The pointer to the method inside the class
         * @param object The instance of the class which the method should be invoked on
         * @return An event handler object, with the method class wrapped into a callable with std::bind
         */
        template<typename class_type>
        static this_type from_method(class_type& object, void (class_type::*method)(argument_types...))
        {
            using binding_type = class_type*;
            using method_type = void (class_type::*)(argument_types...);

            return bind<method_type, binding_type, argument_types...>(method, &object);
        }

        /**
         * Same usage as the other from_method static method, but instead of reference it uses a pointer,
         * so it is easy to use with <code>this<code>
         *
         * @see event_handler<...>::from_method<method_type, class_type>(method_type, class_type&)
         */
        template<typename class_type>
        static this_type from_method(class_type* object, void (class_type::*method)(argument_types...))
        {
            using binding_type = class_type*;
            using method_type = void (class_type::*)(argument_types...);

            return bind<method_type, binding_type, argument_types...>(method, object);
        }

        /**
         * Default constructor, not really useful
         */
        event_handler()
        {
            // Nothing to do yet
        }

        /**
         * The constructor to be used. Accepts everything, accepted by the std::function
         *
         * @param function The function object to be called
         */
        template<typename function_type>
        event_handler(const function_type& function): std::function<void(argument_types...)>(function)
        {
            // Nothing to do yet
        }

        bool operator==(const this_type& other) const
        {
            return other._token == _token;
        }

        const token& get_token() const
        {
            return _token;
        }

    private:
        /**
         * Helper methods to create bindings up to five parameters.
         *
         * If you need more, just add a new static method, with additional template parameters and binding placeholders
         */
        template<typename function_type,
                 typename binding_type>
        static this_type bind(function_type function, binding_type binding)
        {
            return this_type(std::bind(function,
                                       binding));
        }

        template<typename function_type,
                 typename binding_type,
                 typename argument_type_1>
        static this_type bind(function_type function, binding_type binding)
        {
            return this_type(std::bind(function,
                                       binding,
                                       std::placeholders::_1));
        }

        template<typename function_type,
                 typename binding_type,
                 typename argument_type_1,
                 typename argument_type_2>
        static this_type bind(function_type function, binding_type binding)
        {
            return this_type(std::bind(function,
                                       binding,
                                       std::placeholders::_1,
                                       std::placeholders::_2));
        }

        template<typename function_type,
                 typename binding_type,
                 typename argument_type_1,
                 typename argument_type_2,
                 typename argument_type_3>
        static this_type bind(function_type function, binding_type binding)
        {
            return this_type(std::bind(function,
                                       binding,
                                       std::placeholders::_1,
                                       std::placeholders::_2,
                                       std::placeholders::_3));
        }

        template<typename function_type,
                 typename binding_type,
                 typename argument_type_1,
                 typename argument_type_2,
                 typename argument_type_3,
                 typename argument_type_4>
        static this_type bind(function_type function, binding_type binding)
        {
            return this_type(std::bind(function,
                                       binding,
                                       std::placeholders::_1,
                                       std::placeholders::_2,
                                       std::placeholders::_3,
                                       std::placeholders::_4));
        }

        template<typename function_type,
                 typename binding_type,
                 typename argument_type_1,
                 typename argument_type_2,
                 typename argument_type_3,
                 typename argument_type_4,
                 typename argument_type_5>
        static this_type bind(function_type function, binding_type binding)
        {
            return this_type(std::bind(function,
                                       binding,
                                       std::placeholders::_1,
                                       std::placeholders::_2,
                                       std::placeholders::_3,
                                       std::placeholders::_4,
                                       std::placeholders::_5));
        }

        token _token;
};



#endif /* EVENT_HANDLER_HH_ */
