# CPP Event #

CPP Event is a header only C++11 library, which aims to help using events similarly to C# 

### What is this repository for? ###

* Quick summary
    * This repository contains the source code for the library. You can clone this repository
    and use it in your programs
* Version
    * 0.1

[comment]: <> ([Learn Markdown](https://bitbucket.org/tutorials/markdowndemo))

### How do I get set up? ###

* Summary of set up
    * Clone or download the source files
    * Add the directory which contains the sources to your include path (-I for gcc)
    * And that is it. You can include the header files from your source files and use the library
* Configuration
    * No additional configuration needed
* Dependencies
    * CPP Event is a C++11 library, using a few C++11 features, like variadic templates, decltype and so on
* How to run tests
    * Tests are in a separate repository, I did not want to clutter the library with test code

### Who do I talk to? ###

* Repo owner or admin
    * Rudolf Heszele <heszele@gmail.com>

### Example usage ###
    #include <event.hh>
    using event_type = event<>; // Handlers have to have the signature void()
    event_type my_event;
    // event class has a nested type with the proper template arguments to event_handler, called handler
    event_type::handler my_handler([](){  })
    
    my_event += my_handler; // Register listener
    my_event(); // Fire the event
    my_event -= my_handler; // De-register the listener
    my_event += [](){}; // Register a new listener. This listener cannot be removed later on
    event_type::handler my_new_handler = my_event.subscribe([](){}) // Yet another listener
    my_event(); // Only the last two registered listener will be invoked
    my_event.unsubscribe(my_new_handler);
    // Only one listeners remained
    
    // Events can have as many template arguments as needed, but then the handlers
    // need to have the same signature, e.g.:
    event<int, float, double, char> my_event;
    
    event += [](int i, float f, double d, char c){};
    event(5, 5.0f, 5.0, '5');