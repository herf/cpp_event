#ifndef EVENT_HH_
#define EVENT_HH_
#ifdef __EVENT_USE_THREADS__
#include <mutex>
#endif
#include <unordered_map>
#include "event_handler.hh"

/**
 * Variadic class template for event handling, similar to C#.
 * Handlers can register themselves simply by using the += operator
 * or de-register by using the -=.
 * The nested handler class can be used to register and de-register
 * handlers.
 * Alternatively subscribe() and unsubscribe() can also be used.
 * These methods will return a handler object, which can be used
 * to de-register the listener.
 * Handlers can be anything, which can be a std::function.
 * That means, lambda expressions, function pointers and
 * even functors or class methods.
 * For the latter two use the event_handler's helper methods: from_functor() and from_method()
 *
 * Example usage:
 * event<> my_event; // Handlers have to have the signature void()
 * event<>::handler my_handler([](){  })
 *
 * my_event += my_handler; // Register listener
 * my_event(); // Fire the event
 * my_event -= my_handler; // De-register the listener
 * my_event += [](){}; // Register a new listener. This listener cannot be removed later on
 * event<>::handler my_new_handler = my_event.subscribe([](){}) // Yet another listener
 * my_event(); // Only the last two registered listener will be invoked
 * my_event.unsubscribe(my_new_handler);
 * // Only one listeners remained
 *
 * Events can have as many template arguments as needed, but then the handlers
 * need to have the same signature, e.g.:
 * event<int, float, double, char> my_event;
 *
 * event += [](int i, float f, double d, char c){};
 * event(5, 5.0f, 5.0, '5');
 */
template<typename... argument_types>
class event
{
    public:
        using handler = event_handler<argument_types...>;
        using this_type = event<argument_types...>;

        /**
         * Used to register a listener.
         *
         * @param handler The event_handler to be called, when the event is triggered
         */
        this_type& operator+=(const handler& handler)
        {
            {
#ifdef __EVENT_USE_THREADS__
                std::lock_guard<std::mutex> guard(_mutex);
#endif
                if(_event_handlers.end() == _event_handlers.find(handler.get_token()))
                    _event_handlers[handler.get_token()] = handler;
            }

            return *this;
        }

        /**
         * Used to de-register a listener.
         *
         * @param handler The event_handler to be removed
         */
        this_type& operator-=(const handler& handler)
        {
            {
#ifdef __EVENT_USE_THREADS__
                std::lock_guard<std::mutex> guard(_mutex);
#endif
                if(_event_handlers.end() != _event_handlers.find(handler.get_token()))
                    _event_handlers.erase(handler.get_token());
            }

            return *this;
        }

        /**
         * Used to fire the event. The arguments must match the template parameters of the class
         */
        this_type& operator()(argument_types... arguments)
        {
            event_handlers_type event_handlers;

            {
#ifdef __EVENT_USE_THREADS__
                std::lock_guard<std::mutex> guard(_mutex);
#endif
                event_handlers.insert(_event_handlers.begin(), _event_handlers.end());
            }

            for(auto& handler: event_handlers)
                handler.second(arguments...);

            return *this;
        }

        /**
         * Helper method to register a listener. It returns - the possibly generated - event_handler,
         * so later it can be used to de-register the listener
         *
         * @param handler The event_handler to call when the event is fired
         * @return
         */
        handler subscribe(const handler& handler)
        {
            operator+=(handler);

            return handler;
        }

        /**
         * Helper method to de-register a listener.
         *
         * @param handler The event_handler to remove from the listeners
         * @return
         */
        void unsubscribe(const handler& handler)
        {
            operator-=(handler);
        }

    private:
        using event_handlers_type = std::unordered_map<token, handler>;

        event_handlers_type _event_handlers;
#ifdef __EVENT_USE_THREADS__
        std::mutex _mutex;
#endif
};

#endif /* EVENT_HH_ */
